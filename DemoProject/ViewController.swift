//
//  ViewController.swift
//  DemoProject
//
//  Created by Swift on 7/31/20.
//  Copyright © 2020 AnshuTechnologies. All rights reserved.
//

import UIKit

class ViewController: UIViewController, UIScrollViewDelegate {

    var startButton = UIButton()
    var arrowButton = UIButton()
    let screen = UIScreen.main.bounds
    var currenPage = 0
    @IBOutlet weak var scrlView: UIScrollView!
    @IBOutlet weak var pageControl: UIPageControl!
   
    // MARK: ViewLife Cycles
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        designScrollView()
    }
    // MARK: Custom ScrollView Method
    func designScrollView() {
        var count = 0
        var xPos = 0.0
        while count < 4 {
            let lblContent = UILabel(frame: CGRect(x: xPos + 30, y: 30, width: Double(screen.width - 60), height: 100))
            lblContent.numberOfLines = 0
            lblContent.textColor = .white
            let imageView = UIImageView(frame: CGRect(x: xPos, y: 0.0, width: Double(screen.width), height: Double(screen.height)))
            imageView.contentMode = .scaleAspectFit
            if count == 0 {
                imageView.image = UIImage(named: "wscreen1")
                lblContent.text = "Does Your Vehicle Distrupt\nYour Smooth Jouney"
                lblContent.textAlignment = .right
            }
            else if count == 1 {
                imageView.image = UIImage(named: "wscreen2")
                lblContent.text = "We Are Here For\nThe Solution"
                lblContent.textAlignment = .center
            }
            else if count == 2 {
                imageView.image = UIImage(named: "wscreen3")
                lblContent.text = "Toe You And Your Vehicle\nOut Of Trouble"
                lblContent.textAlignment = .right
            }
            else {
                imageView.image = UIImage(named: "wscreen4")
                lblContent.text = ""
                lblContent.textAlignment = .right
                imageView.contentMode = .scaleToFill
            }
            
            scrlView.addSubview(lblContent)
            scrlView.addSubview(imageView)
            
            xPos = xPos + Double(screen.width)
            count = count + 1
        }
        currenPage = 0
        pageControl.numberOfPages = 4
        scrlView.contentSize = CGSize(width: xPos, height: Double(screen.height))

        arrowButton = UIButton(type: .system)
        arrowButton.frame = CGRect(x: screen.width - 90, y: self.view.frame.height - 150, width: 50, height: 50)
        arrowButton.layer.cornerRadius = 0.5 * arrowButton.bounds.size.width
        arrowButton.backgroundColor = .white
        arrowButton.addTarget(self, action: #selector(arrowButtonClicked(sender:)), for: .touchUpInside)
        let image2 = UIImage(named: "right_arrow_purple");
        arrowButton.setImage(image2, for: .normal)
        self.view.addSubview(arrowButton)
        
        startButton = UIButton(type: .system)
        startButton.frame = CGRect(x: screen.width - 100, y: self.view.frame.height - 150, width: 90, height: 50)
        startButton.layer.cornerRadius = 25
        startButton.backgroundColor = .white
        startButton.addTarget(self, action: #selector(startButtonAction(sender:)), for: .touchUpInside)
        startButton.setTitle("Start", for: .normal)
        let image4 = UIImage(named: "right_arrow_purple");
        startButton.setImage(image4, for: .normal)
        self.view.addSubview(startButton)
        startButton.isHidden = true
    }
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let xPos = (scrollView.contentOffset.x)/screen.width
        pageControl.currentPage = Int(xPos)
        if Int(xPos) == 3 {
            startButton.isHidden = false
            arrowButton.isHidden = true
        }
        else {
            startButton.isHidden = true
            arrowButton.isHidden = false
        }
    }
    // MARK: Button Action
    @objc func arrowButtonClicked(sender: UIButton) {
        currenPage = currenPage + 1
        scrlView.setContentOffset(CGPoint(x: currenPage * Int(screen.width), y: 0), animated: true)
        pageControl.currentPage = currenPage
        if currenPage == 3 {
            startButton.isHidden = false
            arrowButton.isHidden = true
        }
        else {
            startButton.isHidden = true
            arrowButton.isHidden = false
        }
    }
    
    @objc func startButtonAction(sender: UIButton) {
    }
}

