import UIKit

class BackGroundGradient: UIView {
    // 6735B9 , 103, 53 185
    // 9B3AAE ,155, 58, 174
    @IBInspectable
   // var startColor: UIColor = .white
    var startColor = UIColor(hexString: "F67062")
    @IBInspectable
   // var endColor: UIColor = .black
    var endColor = UIColor(hexString: "E61D8C")
    private let gradientLayerName = "Gradient"
    override func layoutSubviews() {
        super.layoutSubviews()
        setupGradient()
    }
    private func setupGradient() {
        var gradient: CAGradientLayer? = layer.sublayers?.first { $0.name == gradientLayerName } as? CAGradientLayer
        if gradient == nil {
            gradient = CAGradientLayer()
            gradient?.name = gradientLayerName
            layer.addSublayer(gradient!)
        }
        gradient?.frame = bounds
       // gradient?.colors = [startColor.cgColor, endColor.cgColor]
        gradient?.colors = [startColor?.cgColor as Any, endColor?.cgColor as Any]
        gradient?.zPosition = -1
    }
}
extension UIColor {
    convenience init?(hexString: String) {
        var chars = Array(hexString.hasPrefix("#") ? hexString.dropFirst() : hexString[...])
        let red, green, blue, alpha: CGFloat
        switch chars.count {
        case 3:
            chars = chars.flatMap { [$0, $0] }
            fallthrough
        case 6:
            chars = ["F","F"] + chars
            fallthrough
        case 8:
            alpha = CGFloat(strtoul(String(chars[0...1]), nil, 16)) / 255
            red   = CGFloat(strtoul(String(chars[2...3]), nil, 16)) / 255
            green = CGFloat(strtoul(String(chars[4...5]), nil, 16)) / 255
            blue  = CGFloat(strtoul(String(chars[6...7]), nil, 16)) / 255
        default:
            return nil
        }
        self.init(red: red, green: green, blue:  blue, alpha: alpha)
    }
}
